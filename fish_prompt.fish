function fish_prompt
	set laststatus $status
	set curtime (date|cut -b12-16)
	set prettypwd (string replace "$HOME" '~' "$PWD")
	if type -q git
		set gitbranch (git rev-parse --abbrev-ref HEAD 2> /dev/null)
	end

	echo

	set_color blue
	printf "[$curtime] "

	set_color green
	echo "$prettypwd"

	set_color purple
	if [ -n "$gitbranch" ]
		echo $gitbranch
	end

	if [ $laststatus != 0 ]
		set_color red
		printf "($laststatus) "
	end

	set_color white
	echo '> '
end
